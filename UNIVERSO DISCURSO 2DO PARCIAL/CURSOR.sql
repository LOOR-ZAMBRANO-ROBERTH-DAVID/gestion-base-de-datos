/*-------------------------Cursor que muestra como informe todos los servicios realizados el ultimo mes--------------------*/

--Se declaran variables locales
declare @id_servicio char(4), @cedula_nanny char(10), @apellido_nanny char(20), 
		@nombre_nanny char(20), @fecha_servicio char(10), @total_servicios int = 0, @total_dolares float, @dif varchar (10)

--se declara el cursor con la consulta a la tabla servicio y nanny
declare reporte_cursor cursor
	for select servicio.servicio_id, nanny.nanny_cedula, nanny.nanny_apellido, nanny.nanny_nombre, servicio.servicio_fecha
		from servicio inner join nanny on nanny.nanny_id=servicio.servicio_nanny_id
		order by servicio.servicio_fecha asc

--se apertura el cursor
open reporte_cursor

--se obtiene el primer registro asignado al valor de las columnas en las variables
fetch reporte_cursor into @id_servicio, @cedula_nanny, @apellido_nanny, @nombre_nanny, @fecha_servicio
print 'ID SERVICIO		CEDULA NI�ERA		APELLIDOS NI�ERA		NOMBRES NI�ERA			FECHA DE SERVICIO'
print '---------------------------------------------------------------------------------------------------'


--se implementa la estructura repetitiva WHILE para poder imprimir todos los registros asignados al cursor
while @@FETCH_STATUS=0

begin
	--En la variable diff se almacena la diferencia entre la fecha actual y la fecha de servicio
	set @dif = DATEDIFF (DAY, @fecha_servicio , getdate() )
	--Si la diferencia es mayor a 30, no forma parte del mes
	if(@dif<=30)
	begin
		set @total_servicios+=1
		print space(4)+@id_servicio+space(10)+@cedula_nanny+space(10)+@apellido_nanny+space(5)+ @nombre_nanny+space(5)+ @fecha_servicio
	end
	fetch from reporte_cursor into @id_servicio, @cedula_nanny, @apellido_nanny, @nombre_nanny, @fecha_servicio
end
print' '
--Se imprimen el total de servicios realizados en el mes
print 'EL TOTAL DE SERVICIOS ESTE MES ES: >>' + cast(@total_servicios as char(6))

close reporte_cursor
deallocate reporte_cursor
go

/*--------------------------------------------------------------------------------------------------------*/

