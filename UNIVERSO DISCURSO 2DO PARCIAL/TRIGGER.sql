
/*-----------Triigger que permite registrar unicamente a niñeras mayores de edad------------*/
create trigger tr_edad_nanny on dbo.nanny instead of insert
as
/*Se declara la variable para la edad*/
declare @edad int;

/*Calculo de edad*/
set @edad= (select (datediff(year, nanny_fecha_nacimiento , getdate())) from inserted);

if(@edad<18)
begin
	raiserror ('SOLO SE PUEDEN INGRESAR PERSONAS CON EDAD MAYOR O IGUAL A 18 AÑOS',16,217) with log;
end

if(@edad>65)
begin
	raiserror ('SOLO SE PUEDEN INGRESAR PERSONAS CON EDAD MENOR O IGUAL A 65 AÑOS',16,217) with log;
end

if(@edad >= 18 and @edad<=65)
begin
	insert nanny select * from inserted
end
go
/*--------------------------------------------------------------------------------------------------------*/


