/*-------- APROCESO ALMACENADO QUE MUESTRE DATOS DEL PADRE DE UN HIJO Y SUS APORTACIONES-------*/

---Creamos el proceso almacenado
create proc p_datos_padres
--Declaramos una variable local la cual se encargara de almacenar el parametro de entrada
@id_hijo char (4)
as
--Creamos la consulta que sera la base del proceso almacenado
		   select hijo.hijo_id,
		   hijo.hijo_nombre,
		   cliente.cliente_cedula,
		   cliente.cliente_nombre,
		   cliente.cliente_apellido,
		   cliente.cliente_cedula,
		   sum(sevricio_pago_realizado) as aportacion_dolares

		   
		   from servicio inner join cliente on cliente_id = servicio_cliente_id 
		   inner join hijo on cliente_id = hijo_cliente_id
		   where hijo.hijo_id = @id_hijo
		   group by hijo.hijo_id, hijo_nombre, cliente.cliente_cedula, cliente.cliente_nombre, cliente.cliente_apellido, cliente.cliente_cedula 


--ejecutamos el proceso almacenado y como parametro se le pasara el id de uno de los hijos

exec p_datos_padres 'H007';
/*--------------------------------------------------------------------------------------------------------*/





